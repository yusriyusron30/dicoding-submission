package com.yusriyusron.tvmovies.controller;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.yusriyusron.tvmovies.R;
import com.yusriyusron.tvmovies.controller.fragment.MoviesFragment;
import com.yusriyusron.tvmovies.controller.fragment.TvShowsFragment;

public class ContentAdapter extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 2;
    private Context context;

    public ContentAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if (position == 0){
            fragment = new MoviesFragment();
        }else {
            fragment = new TvShowsFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return context.getResources().getString(R.string.title_movie);
            case 1:
                return context.getResources().getString(R.string.title_tv);
        }
        return null;
    }
}
